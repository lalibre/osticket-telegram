<?php

return array(
    'id' =>             'osticket:telegrambot',
    'version' =>        '0.3.1',
    'name' =>           'Telegram Bot',
    'author' =>         'Tomas Kirkegaard',
    'description' =>    'Notify Telegram on new ticket.',
    'url' =>            'https://gitlab.com/jfinlay/osticketTelegrambot',
    'plugin' =>         'telegrambot.php:TelegramPlugin',
);

?>
