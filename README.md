osTicket-telegram
==============
An plugin for [osTicket](https://osticket.com) which posts notifications to a [Telegram](https://telegram.org) channel/chat/group.

## Note
This project is maintained for me, and tested on 1.15.2.

Install
--------
Clone this repo or download the zip file and place the contents into your `include/plugins` folder.
Insert Telegrams Bot URL for your bot (ex. `https://api.telegram.org/bot<token>/`) and Chat ID (only numbers).

For more information about Telegram Bot, see: https://core.telegram.org/bots/api

Info
------
This plugin uses CURL and tested on osTicket-1.15.2

Based on [thammanna/osticket-slack](https://github.com/thammanna/osticket-slack)
